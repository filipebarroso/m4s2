// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'riverpod_provider_examples.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$counterHash() => r'ed59073069d03f8ace971108d5204fb62c80f40c';

/// 1. PROVIDER
///
/// Copied from [counter].
@ProviderFor(counter)
final counterProvider = AutoDisposeProvider<int>.internal(
  counter,
  name: r'counterProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$counterHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CounterRef = AutoDisposeProviderRef<int>;
String _$counterFutureHash() => r'd266df2184254d6f16e8b5b9387acfd5cd0cc21a';

/// 2. FUTURE
///
/// Copied from [counterFuture].
@ProviderFor(counterFuture)
final counterFutureProvider = AutoDisposeFutureProvider<int>.internal(
  counterFuture,
  name: r'counterFutureProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$counterFutureHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CounterFutureRef = AutoDisposeFutureProviderRef<int>;
String _$counterStreamHash() => r'b220b6f05439213896c08aedf40207fa28a47a60';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

typedef CounterStreamRef = AutoDisposeStreamProviderRef<String>;

/// 3. STREAMS
///
/// Copied from [counterStream].
@ProviderFor(counterStream)
const counterStreamProvider = CounterStreamFamily();

/// 3. STREAMS
///
/// Copied from [counterStream].
class CounterStreamFamily extends Family<AsyncValue<String>> {
  /// 3. STREAMS
  ///
  /// Copied from [counterStream].
  const CounterStreamFamily();

  /// 3. STREAMS
  ///
  /// Copied from [counterStream].
  CounterStreamProvider call({
    Duration period = const Duration(seconds: 3),
    String moneyIcon = '£',
  }) {
    return CounterStreamProvider(
      period: period,
      moneyIcon: moneyIcon,
    );
  }

  @override
  CounterStreamProvider getProviderOverride(
    covariant CounterStreamProvider provider,
  ) {
    return call(
      period: provider.period,
      moneyIcon: provider.moneyIcon,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'counterStreamProvider';
}

/// 3. STREAMS
///
/// Copied from [counterStream].
class CounterStreamProvider extends AutoDisposeStreamProvider<String> {
  /// 3. STREAMS
  ///
  /// Copied from [counterStream].
  CounterStreamProvider({
    this.period = const Duration(seconds: 3),
    this.moneyIcon = '£',
  }) : super.internal(
          (ref) => counterStream(
            ref,
            period: period,
            moneyIcon: moneyIcon,
          ),
          from: counterStreamProvider,
          name: r'counterStreamProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$counterStreamHash,
          dependencies: CounterStreamFamily._dependencies,
          allTransitiveDependencies:
              CounterStreamFamily._allTransitiveDependencies,
        );

  final Duration period;
  final String moneyIcon;

  @override
  bool operator ==(Object other) {
    return other is CounterStreamProvider &&
        other.period == period &&
        other.moneyIcon == moneyIcon;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, period.hashCode);
    hash = _SystemHash.combine(hash, moneyIcon.hashCode);

    return _SystemHash.finish(hash);
  }
}

String _$counterClassHash() => r'b27fb9c4f2965dcb0cc467ccf4db279ae627f7eb';

/// 4. CLASS
///
/// Copied from [CounterClass].
@ProviderFor(CounterClass)
final counterClassProvider =
    AutoDisposeNotifierProvider<CounterClass, dynamic>.internal(
  CounterClass.new,
  name: r'counterClassProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$counterClassHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$CounterClass = AutoDisposeNotifier<dynamic>;
// ignore_for_file: unnecessary_raw_strings, subtype_of_sealed_class, invalid_use_of_internal_member, do_not_use_environment, prefer_const_constructors, public_member_api_docs, avoid_private_typedef_functions
