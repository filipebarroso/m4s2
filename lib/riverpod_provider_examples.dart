import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'riverpod_provider_examples.g.dart';

/// 1. PROVIDER
@riverpod
int counter(CounterRef ref) => 43;

/// 2. FUTURE
@riverpod
Future<int> counterFuture(CounterFutureRef ref) async {
  return Future.delayed(const Duration(seconds: 3), () => 42);
}

/// 3. STREAMS
@riverpod
Stream<String> counterStream(
  CounterStreamRef ref, {
  Duration period = const Duration(seconds: 3),
  String moneyIcon = '£',
}) async* {
  await for (final value in Stream.periodic(period, (index) => index)) {
    yield '$moneyIcon$value';
  }
}

/// 4. CLASS
@riverpod
class CounterClass extends _$CounterClass {
  @override
  build() => 42;

  void increment() {
    state++;
  }
}

class RiverpodProviderExamplesPage extends ConsumerWidget {
  const RiverpodProviderExamplesPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final counterClass = ref.watch(counterClassProvider);

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Count: $counterClass ',
              style: Theme.of(context).textTheme.displayLarge,
            ),
            ElevatedButton(
                onPressed: () {
                  //
                  ref.read(counterClassProvider.notifier).increment();
                },
                child: const Text('+'))
          ],
        ),
      ),
    );

    final counterStream = ref.watch(
      counterStreamProvider.call(
        period: const Duration(seconds: 1),
      ),
    );

    return Scaffold(
      body: Center(
        child: counterStream.when(
          data: (data) => Text(
            'counter: $data',
            style: Theme.of(context).textTheme.displayLarge,
          ),
          loading: () => const CircularProgressIndicator(),
          error: (error, stackTrace) => Text('Error: $error'),
        ),
      ),
    );

    final counterAsyncValue = ref.watch(counterFutureProvider);
    return Scaffold(
      body: Center(
        child: counterAsyncValue.when(
          data: (data) => Text(
            'counter: $data',
            style: Theme.of(context).textTheme.displayLarge,
          ),
          loading: () => const CircularProgressIndicator(),
          error: (error, stackTrace) => Text('Error: $error'),
        ),
      ),
    );

    final counter = ref.watch(counterProvider);
    return Scaffold(
      body: Center(
        child: Text(
          'Count: $counter ',
          style: Theme.of(context).textTheme.displayLarge,
        ),
      ),
    );
  }
}
