import 'package:app_francesinha/public/features/application/app.dart';
import 'package:app_francesinha/public/features/cart/application/bloc/cart_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp(
    ProviderScope(
      // overrides: const [],
      child: EasyLocalization(
        supportedLocales: const [
          Locale('en', 'US'),
          Locale('pt', 'PT'),
        ],
        path: 'assets/translations',
        fallbackLocale: const Locale('pt', 'PT'),
        child: MultiBlocProvider(
          providers: [
            BlocProvider<CartBloc>(create: (context) => CartBloc()),
          ],
          child: const RestaurantApp(),
        ),
      ),
    ),
  );
}
