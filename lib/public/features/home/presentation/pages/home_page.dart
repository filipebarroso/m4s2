import 'package:app_francesinha/public/features/cart/presentation/cart_page_view.dart';
import 'package:app_francesinha/public/features/home/data/home_repository.dart';
import 'package:app_francesinha/public/features/home/presentation/home_page_view.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key, this.homeRepository});

  final HomeRepository? homeRepository;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: LayoutBuilder(
        builder: (context, constraints) {
          final isDesktop = constraints.maxWidth > 1100;
          return Scaffold(
            appBar: AppBar(
              title: Text(isDesktop ? 'Restaurante' : 'Menu'),
              bottom: isDesktop
                  ? null
                  : const TabBar(
                      tabs: [
                        Tab(icon: Icon(Icons.menu_book)),
                        Tab(icon: Icon(Icons.shopping_bag)),
                      ],
                    ),
            ),
            body: isDesktop
                ? Row(
                    children: [
                      Expanded(
                        child: HomePageView(
                          items: homeRepository?.getAllFoodItems() ?? [],
                        ),
                      ),
                      const SizedBox(width: 18),
                      const Expanded(child: CartPageView()),
                    ],
                  )
                : TabBarView(
                    children: [
                      HomePageView(
                        items: homeRepository?.getAllFoodItems() ?? [],
                      ),
                      const CartPageView(),
                    ],
                  ),
          );
        },
      ),
    );
  }
}
