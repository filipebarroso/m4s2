import 'package:app_francesinha/public/features/application/navigations.dart';
import 'package:flutter/material.dart';

// All methods to help navigate the app
extension NavigationExtensions on BuildContext {
  void navigateToDetailPage() {
    Navigator.of(this).pushNamed(
      AppNavigationPaths.detail,
    );
  }
}
