import 'package:app_francesinha/public/features/home/domain/food_item.dart';
import 'package:flutter/material.dart';

class SelectedFoodInheritedWidget extends InheritedWidget {
  final SelectedFood selected;

  const SelectedFoodInheritedWidget({
    super.key,
    required super.child,
    required this.selected,
  });

  @override
  bool updateShouldNotify(covariant SelectedFoodInheritedWidget oldWidget) {
    return selected.food != (oldWidget).selected.food;
  }

  static SelectedFoodInheritedWidget? _maybeOf(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<SelectedFoodInheritedWidget>();
  }

  static SelectedFoodInheritedWidget of(BuildContext context) {
    final inherited = _maybeOf(context);

    return inherited!;
  }
}

class SelectedFood {
  FoodItem? _food;

  SelectedFood(this._food);

  // Named constructor
  SelectedFood.nonePicked() : _food = null;

  FoodItem? get food => _food;

  void setFood(FoodItem food) {
    _food = food;
  }
}
